define(['jquery'], function($){
    var CustomWidget = function () {
    	var self = this,
            selected_leads_id = [];

         this.download_csv = function(filename, content) {
            var link, csv;

            csv = encodeURI('data:text/csv;charset=utf-8,' + content);

            link = document.createElement('a');
            link.setAttribute('href', csv);
            link.setAttribute('download', filename);
            link.click();
        }

        this.convert_data_to_csv = function(headers, data) {
            var result = '',
                column_delimeter = ',';
                line_delimeter = '\n';

                result += Object.values(headers).join(column_delimeter);

                var keys = Object.keys(headers);

                data.forEach(function(item) {
                    var i = 0;

                    result += line_delimeter;

                    keys.forEach(function(key) {
                        if (i > 0) {
                            result += column_delimeter;
                        }
                        result += '"' + item[key] + '"';
                        i++;
                    })
                })

                return result;
        }

        this.export_to_csv = function(leads_id) {
            var data = [];
            $.get('/api/v2/leads', { id: leads_id }, function(res) {
                var data = [];

                var contacts_id = [];
                var companies_id = [];

                res._embedded.items.forEach(function(lead) {
                    console.log(lead);

                    var tags = '',
                        custom_fields = '';

                    if (Array.isArray(lead.tags)) {
                        tags = lead.tags.map(function(tag) { return tag.name }).join(', ');
                    }

                    if (Array.isArray(lead.custom_fields)) {
                        custom_fields = lead.custom_fields.map(function(custom_field) {
                            return custom_field.name + ' - ' + custom_field.values.map(function(value) {
                                return value.value;
                            }).join(', ')
                        }).join(';')
                    }

                    var item = {
                        name: lead.name,
                        created_at: lead.created_at,
                        contacts: [],
                        company_name: '',
                        tags: tags,
                        custom_fields: custom_fields
                    };

                    if (lead.company) {
                        item.company_id = lead.company.id;

                        if (companies_id.indexOf(lead.company.id) == -1) {
                            companies_id.push(lead.company.id)
                        }
                    }

                    if (lead.contacts.id) {
                        item.contacts_id = lead.contacts.id;

                        lead.contacts.id.forEach(function(id) {
                            if (contacts_id.indexOf(id) == -1) {
                                contacts_id.push(id)
                            }
                        })
                    }

                    data.push(item);
                })

                $.when($.get('/api/v2/contacts', { id: contacts_id }),
                    $.get('/api/v2/companies', { id: companies_id })
                ).then(function(res1, res2) {
                    data.forEach(function(item) {
                        res1[0]._embedded.items.forEach(function(contact) {
                            if (item.contacts_id.indexOf(contact.id) != -1) {
                                item.contacts.push(contact.name);
                            }
                        });

                        item.contacts = item.contacts.join(', ');

                        res2[0]._embedded.items.forEach(function(company) {
                            if (item.company_id == company.id) {
                                item.company_name = company.name;
                            }
                        })
                    })

                    headers = {
                        name: self.i18n('widget.csv_headers.name'),
                        created_at: self.i18n('widget.csv_headers.created_at'),
                        tags: self.i18n('widget.csv_headers.tags'),
                        custom_fields: self.i18n('widget.csv_headers.custom_fields'),
                        contacts: self.i18n('widget.csv_headers.contacts'),
                        company_name: self.i18n('widget.csv_headers.company_name')
                    };

                    var content = self.convert_data_to_csv(headers, data);
                    self.download_csv('export.csv', content);
                })
            })

        }
		this.callbacks = {
			render: function(){
                self.render_template({
                        caption:{
                        class_name:'new_widget',
                    },
                    body: self.render({ref: '/tmpl/controls/button.twig'}, {
                        text: self.i18n('widget.export_csv_button_text'),
                        class_name:'js-export-leads'
                    }),
                    render : ''
                });

				return true;
			},
			init: function(){
				console.log('init');
				return true;
			},
			bind_actions: function(){
				console.log('bind_actions');
                $('.js-export-leads').on('click', function () {
                    self.export_to_csv(selected_leads_id)
                });

				return true;
			},
			settings: function(){

			},
			onSave: function(){
				alert('click');
				return true;
			},
			destroy: function(){

			},
			leads: {
					//select leads in list and clicked on widget name
					selected: function(){
						console.log('leads');
                        selected_leads_id = []
                        var selected_leads = self.list_selected().selected;

                        selected_leads.forEach(function(item) {
                            console.log(item);
                            selected_leads_id.push(item.id)
                        })
					}
				}
		};
		return this;
    };

return CustomWidget;
});
